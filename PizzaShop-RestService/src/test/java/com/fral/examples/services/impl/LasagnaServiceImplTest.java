package com.fral.examples.services.impl;

import com.fral.examples.common.utils.ExpectedResult;
import com.fral.examples.config.SpringTestContext;
import com.fral.examples.dal.model.Lasagna;
import com.fral.examples.dal.model.Product;
import com.fral.examples.dal.repository.api.LasagnaRepository;
import com.fral.examples.dal.repository.api.OrderRepository;
import com.fral.examples.exception.OrderNotFoundException;
import com.fral.examples.services.builder.LasagnaBuilder;
import com.fral.examples.services.builder.ProductBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author franco.robert.fral@gmail.com
 */

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = SpringTestContext.class)
public class LasagnaServiceImplTest {

    @Mock
    private LasagnaRepository lasagnaRepositoryMock;

    @Mock
    private OrderRepository orderRepositoryMock;

    @Spy
    private ProductBuilder lasagnaBuilderMock;

    @Autowired
    @InjectMocks
    private LasagnaServiceImpl lasagnaService;



    @Before
    public void setUp() throws Exception {
        lasagnaBuilderMock = spy(LasagnaBuilder.class);

        MockitoAnnotations.initMocks(this);

        //Stubbing for Lasagna Order creation.
        stub(orderRepositoryMock.findOne(1L)).toReturn(ExpectedResult.getExpectedOrder());
        stub(lasagnaRepositoryMock.save(any(Lasagna.class))).toReturn(ExpectedResult.getExpectedLasagna());
        doCallRealMethod().when(lasagnaBuilderMock).getProduct();

        //Stubbing for finding Lasagna expected Result.
        stub(lasagnaRepositoryMock.findOne(1L)).toReturn(ExpectedResult.getExpectedLasagna());
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void takeTheOrder_LasagnaOrderCreation_Success() throws Exception {

        Product lasagnaProduct = lasagnaService.takeTheOrder(1L, 3);

        assertNotNull("Expected result is not NULL", lasagnaProduct);
        assertEquals(Lasagna.class, lasagnaProduct.getClass());
        assertTrue(lasagnaProduct instanceof Product);
    }

    @Test(expected = OrderNotFoundException.class)
    public void takeTheOrder_LasagnaOrderCreation_FailOrderNotFound() {
        Product lasagnaProduct = lasagnaService.takeTheOrder(50L, 3);
    }

    @Test
    public void findById_GetLasagnaWithExistingId_Success() throws Exception {

        Product lasagnaProduct = lasagnaService.findById(1L);

        assertNotNull("Not null product is expected", lasagnaProduct);
        assertEquals(Lasagna.class, lasagnaProduct.getClass());
        assertTrue(lasagnaProduct instanceof Product);
    }

    @Test
    public void findById_GetLasagnaWithWrongId_NullIsExpected() {
        Product lasagnaProduct = lasagnaService.findById(50L);

        assertNull("Incorrect lasagna ID, so null is expected", lasagnaProduct);
    }

}