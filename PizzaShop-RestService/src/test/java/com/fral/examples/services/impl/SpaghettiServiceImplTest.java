package com.fral.examples.services.impl;

import com.fral.examples.common.utils.ExpectedResult;
import com.fral.examples.config.SpringTestContext;
import com.fral.examples.dal.model.Product;
import com.fral.examples.dal.model.Spaghetti;
import com.fral.examples.dal.repository.api.OrderRepository;
import com.fral.examples.dal.repository.api.SpaghettiRepository;
import com.fral.examples.exception.OrderNotFoundException;
import com.fral.examples.services.builder.ProductBuilder;
import com.fral.examples.services.builder.SpaghettiBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.stub;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = SpringTestContext.class)
public class SpaghettiServiceImplTest {

    @Mock
    private SpaghettiRepository spaghettiRepositoryMock;

    @Mock
    private OrderRepository orderRepositoryMock;

    @Spy
    private ProductBuilder spaghettiBuilderMock;

    @Autowired
    @InjectMocks
    private SpaghettiServiceImpl spaghettiService;


    @Before
    public void setUp() throws Exception {
        spaghettiBuilderMock = spy(SpaghettiBuilder.class);

        MockitoAnnotations.initMocks(this);

        //Stubbing for Spaghetti Order creation
        stub(orderRepositoryMock.findOne(1L)).toReturn(ExpectedResult.getExpectedOrder());
        stub(spaghettiRepositoryMock.save(any(Spaghetti.class))).toReturn(ExpectedResult.getExpectedSpaghetti());
        doCallRealMethod().when(spaghettiBuilderMock).getProduct();

        //Stubbing for finding Spaghetti expected result.
        stub(spaghettiRepositoryMock.findOne(4L)).toReturn(ExpectedResult.getExpectedSpaghetti());
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void takeTheOrder_SpaghettiOrderCreation_Success() throws Exception {

        Product spaghettiProduct = spaghettiService.takeTheOrder(1L, 8);

        assertNotNull("Expected result is not NULL", spaghettiProduct);
        assertEquals(Spaghetti.class, spaghettiProduct.getClass());
        assertTrue(spaghettiProduct instanceof Product);
    }

    @Test(expected = OrderNotFoundException.class)
    public void takeTheOrder_SpaghettiOrderCreation_FailOrderNotFound() {
        Product spaghettiProduct = spaghettiService.takeTheOrder(50L, 8);
    }

    @Test
    public void findById_GetSpaghettiWithExistingId_Success() throws Exception {
        Product spaghettiProduct = spaghettiService.findById(4L);

        assertNotNull("Not null product is expected", spaghettiProduct);
        assertEquals(Spaghetti.class, spaghettiProduct.getClass());
        assertTrue(spaghettiProduct instanceof Product);
    }

    public void findById_GetSpaghettiWithWrongId_NullIsExpected() {
        Product spaghettiProduct = spaghettiService.findById(50L);

        assertNull("Incorrect lasagna ID, so null is expected", spaghettiProduct);
    }

}