package com.fral.examples.services.impl;

import com.fral.examples.common.utils.ExpectedResult;
import com.fral.examples.config.SpringTestContext;
import com.fral.examples.dal.model.Product;
import com.fral.examples.dal.model.Salad;
import com.fral.examples.dal.repository.api.OrderRepository;
import com.fral.examples.dal.repository.api.SaladsRepository;
import com.fral.examples.exception.OrderNotFoundException;
import com.fral.examples.services.builder.ProductBuilder;
import com.fral.examples.services.builder.SaladBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.stub;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = SpringTestContext.class)
public class SaladServiceImplTest {

    @Mock
    private SaladsRepository saladsRepositoryMock;

    @Mock
    private OrderRepository orderRepositoryMock;

    @Spy
    private ProductBuilder saladBuilderMock;

    @Autowired
    @InjectMocks
    private SaladServiceImpl saladService;



    @Before
    public void setUp() throws Exception {
        saladBuilderMock = spy(SaladBuilder.class);

        MockitoAnnotations.initMocks(this);

        //Stubbing for Salad Order creation
        stub(orderRepositoryMock.findOne(1L)).toReturn(ExpectedResult.getExpectedOrder());
        stub(saladsRepositoryMock.save(any(Salad.class))).toReturn(ExpectedResult.getExpectedSalad());
        doCallRealMethod().when(saladBuilderMock).getProduct();

        //Stubbing for finding Salad expected Result
        stub(saladsRepositoryMock.findOne(5L)).toReturn(ExpectedResult.getExpectedSalad());
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void takeTheOrder_SaladOrderCreation_Success() throws Exception {
        Product saladProduct = saladService.takeTheOrder(1L, 1);

        assertNotNull("Expected result is not NULL", saladProduct);
        assertEquals(Salad.class, saladProduct.getClass());
        assertTrue(saladProduct instanceof Product);
    }

    @Test(expected = OrderNotFoundException.class)
    public void takeTheOrder_SaladOrderCreation_FailOrderNotFound() {
        Product saladProduct = saladService.takeTheOrder(50L, 5);
    }

    @Test
    public void findById_GetSaladWithExistingId_Success() throws Exception {
        Product saladProduct = saladService.findById(5L);

        assertNotNull("Not null product is expected", saladProduct);
        assertEquals(Salad.class, saladProduct.getClass());
        assertTrue(saladProduct instanceof Product);
    }

    @Test
    public void findById_GetSaladWithWrongId_NullIsExpected() {
        Product saladProduct = saladService.findById(50L);

        assertNull("Incorrect salad ID, so null is expected", saladProduct);
    }
}