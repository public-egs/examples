package com.fral.examples.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author franco.robert.fral@gmail.com
 */

@Configuration
@ComponentScan(basePackages = {
        "com.fral.examples.services.api",
        "com.fral.examples.services.impl",
        "com.fral.examples.services.builder"
})
public class SpringTestContext {
}
