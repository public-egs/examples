package com.fral.examples.common.utils;

import com.fral.examples.dal.model.*;

import java.util.*;

/**
 * @author franco.robert.fral@gmail.com
 */
public class ExpectedResult {

    public static final Date creationOrderDate = Calendar.getInstance().getTime();

    public static Order getExpectedOrder() {
        Order expectedOrder = new Order();
        expectedOrder.setId(1L);
        expectedOrder.setStatus(OrderStatus.PENDING);
        expectedOrder.setOrderDetail("Order Detail");
        expectedOrder.setOrderDate(creationOrderDate);

        return expectedOrder;
    }

    public static Lasagna getExpectedLasagna() {
        Lasagna expectedLasagna = new Lasagna();
        expectedLasagna.setId(1L);
        expectedLasagna.setPrice(90.0);
        expectedLasagna.setDetail("PRODUCT: Lasagna - DESCRIPTION: Lasagna order has been registered. - AMOUNT: 3 - UNIT PRICE: 90.0 - TOTAL COST: 270.0");
        expectedLasagna.setName("Lasagna");
        expectedLasagna.setAmount(3);
        expectedLasagna.setTimeToBePrepared(40L);
        expectedLasagna.setMeat("Lasagna Mee");
        expectedLasagna.setSausage("Lasagna Sausage");
        expectedLasagna.setOlives("Lasagna Olive");
        expectedLasagna.setPasta("Lasagna Pasta");
        expectedLasagna.setGarlic("Lasagna Garlic");
        expectedLasagna.setPepper("Lasagna Pepper");
        expectedLasagna.setOregano("Lasagna Oregano");
        expectedLasagna.setOrder(getExpectedOrder());

        return expectedLasagna;
    }

    public static Salad getExpectedSalad() {
        Salad expectedSalad = new Salad();
        expectedSalad.setId(5L);
        expectedSalad.setPrice(40.0);
        expectedSalad.setDetail("PRODUCT: Salad - DESCRIPTION: Salad order has been registered. - AMOUNT: 1 - UNIT PRICE: 40.0 - TOTAL COST: 40.0");
        expectedSalad.setName("Salad");
        expectedSalad.setAmount(1);
        expectedSalad.setTimeToBePrepared(15L);
        expectedSalad.setQuinoa("Salad Quinoa");
        expectedSalad.setOil("Salad Oil");
        expectedSalad.setVinegar("Salad Vinegar");
        expectedSalad.setSalt("Salad salt");
        expectedSalad.setOnion("Salad Onion");
        expectedSalad.setOrder(getExpectedOrder());

        return expectedSalad;
    }

    public static Spaghetti getExpectedSpaghetti() {
        Spaghetti expectedSpaghetti = new Spaghetti();
        expectedSpaghetti.setId(4L);
        expectedSpaghetti.setPrice(120.0);
        expectedSpaghetti.setDetail("PRODUCT: Spaghetti - DESCRIPTION: Spaghetti order has been registered. - AMOUNT: 8 - UNIT PRICE: 120.0 - TOTAL COST: 960.0");
        expectedSpaghetti.setName("Spaghetti");
        expectedSpaghetti.setAmount(8);
        expectedSpaghetti.setTimeToBePrepared(60L);
        expectedSpaghetti.setMeat("Spaghetti Meat");
        expectedSpaghetti.setJitomates("Spaghetti Jitomates");
        expectedSpaghetti.setPuree("Spaghetti Pure");
        expectedSpaghetti.setPasta("Spaghetti Pasta");
        expectedSpaghetti.setOil("Spaghetti Oil");
        expectedSpaghetti.setOrder(getExpectedOrder());

        return expectedSpaghetti;
    }

    public static Pizza getExpectedVegetarianPizza() {
        Pizza expectedPizza = new Pizza();
        expectedPizza.setId(3L);
        expectedPizza.setPrice(15.0);
        expectedPizza.setDetail("PRODUCT: Veggie Pizza - DESCRIPTION: Pizza size: PERSONAL(2 slides) - AMOUNT: 2 - UNIT PRICE: 15.0 - TOTAL COST: 30.0");
        expectedPizza.setName("Veggie Pizza");
        expectedPizza.setAmount(2);
        expectedPizza.setTimeToBePrepared(30L);
        expectedPizza.setCheese("Vegetarian Cheese");
        expectedPizza.setSauce("Vegetarian Sauce");
        expectedPizza.setCrust("Vegetarian Crust");
        expectedPizza.setSlides(2);
        expectedPizza.setPizzaType(PizzaType.VEGGIE);
        expectedPizza.setCustomizedIngredients(getCustomizedIngredients());
        expectedPizza.setOrder(getExpectedOrder());

        return expectedPizza;
    }

    public static Set<ExtraIngredient> getCustomizedIngredients() {
        Set<ExtraIngredient> extraIngredients = new HashSet<>();

        ExtraIngredient extraIngredient = new ExtraIngredient();
        extraIngredient.setId(2L);
        extraIngredient.setExtraTopping("grains of corn");

        return extraIngredients;
    }
}
