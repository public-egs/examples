package com.fral.examples.dal.model;

import com.fral.examples.common.utils.PizzaType;

import javax.persistence.*;
import java.util.Set;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Entity
@Table(name = "pizza")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "pizza_id", referencedColumnName = "product_id")
})
public class Pizza extends Product {

    //region Fields
    @Column(name = "cheese")
    private String cheese;

    @Column(name = "sauce")
    private String sauce;

    @Column(name = "crust")
    private String crust;

    @Column(name = "slides")
    private int slides;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private PizzaType pizzaType;

    @Column(name = "custom_ingredients")
    @OneToMany(cascade = { CascadeType.PERSIST })
    private Set<ExtraIngredient> customizedIngredients;

    //endregion

    //region Getters & Setters
    public String getCheese() {
        return cheese;
    }

    public void setCheese(String cheese) {
        this.cheese = cheese;
    }

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public String getCrust() {
        return crust;
    }

    public void setCrust(String crust) {
        this.crust = crust;
    }

    public int getSlides() {
        return slides;
    }

    public void setSlides(int slides) {
        this.slides = slides;
    }

    public PizzaType getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(PizzaType pizzaType) {
        this.pizzaType = pizzaType;
    }

    public Set<ExtraIngredient> getCustomizedIngredients() {
        return customizedIngredients;
    }

    public void setCustomizedIngredients(Set<ExtraIngredient> customizedIngredients) {
        this.customizedIngredients = customizedIngredients;
    }

    //endregion
}
