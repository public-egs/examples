package com.fral.examples.dal.model;

import javax.persistence.*;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Entity
@Table(name = "spaghetti")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "spaghetti_id", referencedColumnName = "product_id")
})
public class Spaghetti extends Product {

    //region Fields
    @Column(name = "meat")
    private String meat;

    @Column(name = "jitomates")
    private String jitomates;

    @Column(name = "puree")
    private String puree;

    @Column(name = "pasta")
    private String pasta;

    @Column(name = "oil")
    private String oil;
    //endregion

    //region Getters & Setters

    public String getMeat() {
        return meat;
    }

    public void setMeat(String meat) {
        this.meat = meat;
    }

    public String getJitomates() {
        return jitomates;
    }

    public void setJitomates(String jitomates) {
        this.jitomates = jitomates;
    }

    public String getPuree() {
        return puree;
    }

    public void setPuree(String puree) {
        this.puree = puree;
    }

    public String getPasta() {
        return pasta;
    }

    public void setPasta(String pasta) {
        this.pasta = pasta;
    }

    public String getOil() {
        return oil;
    }

    public void setOil(String oil) {
        this.oil = oil;
    }

    //endregion
}
