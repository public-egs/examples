package com.fral.examples.dal.repository.api;

import com.fral.examples.common.utils.OrderStatus;
import com.fral.examples.dal.model.Order;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Component
@Scope("prototype")
public interface OrderRepository extends JpaRepository<Order, Long> {

    Collection<Order> findByStatus(OrderStatus status);
}
