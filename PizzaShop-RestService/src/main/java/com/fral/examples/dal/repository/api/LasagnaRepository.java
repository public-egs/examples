package com.fral.examples.dal.repository.api;

import com.fral.examples.dal.model.Lasagna;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Component
@Scope("prototype")
public interface LasagnaRepository extends JpaRepository<Lasagna, Long> {
}
