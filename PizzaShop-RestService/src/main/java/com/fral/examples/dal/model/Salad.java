package com.fral.examples.dal.model;

import javax.persistence.*;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Entity
@Table(name = "salad")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "salad_id", referencedColumnName = "product_id")
})
public class Salad extends Product {

    //region Fields
    @Column(name = "quinoa")
    private String quinoa;

    @Column(name = "oil")
    private String oil;

    @Column(name = "vinegar")
    private String vinegar;

    @Column(name = "salt")
    private String salt;

    @Column(name = "onion")
    private String onion;
    //endregion

    //region Getters & Setters

    public String getQuinoa() {
        return quinoa;
    }

    public void setQuinoa(String quinoa) {
        this.quinoa = quinoa;
    }

    public String getOil() {
        return oil;
    }

    public void setOil(String oil) {
        this.oil = oil;
    }

    public String getVinegar() {
        return vinegar;
    }

    public void setVinegar(String vinegar) {
        this.vinegar = vinegar;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getOnion() {
        return onion;
    }

    public void setOnion(String onion) {
        this.onion = onion;
    }

    //endregion
}
