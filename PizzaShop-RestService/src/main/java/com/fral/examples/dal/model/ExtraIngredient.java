package com.fral.examples.dal.model;

import javax.persistence.*;

/**
 * @author: franco.robert.fral@gmail.com.
 */

@Entity
@Table(name = "extra_ingredient")
public class ExtraIngredient {

    //region Fields
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ingredient_id", nullable = false, unique = true)
    private Long id;

    @Column(name = "extra_topping")
    private String extraTopping;
    //endregion

    //region Getters & Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExtraTopping() {
        return extraTopping;
    }

    public void setExtraTopping(String extraTopping) {
        this.extraTopping = extraTopping;
    }
    //endregion
}
