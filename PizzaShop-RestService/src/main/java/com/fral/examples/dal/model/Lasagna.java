package com.fral.examples.dal.model;

import javax.persistence.*;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Entity
@Table(name = "lasagna")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "lasagna_id", referencedColumnName = "product_id")
})
public class Lasagna extends Product {

    //region Fields
    @Column(name = "meat")
    private String meat;

    @Column(name = "sausage")
    private String sausage;

    @Column(name = "olives")
    private String olives;

    @Column(name = "pasta")
    private String pasta;

    @Column(name = "garlic")
    private String garlic;

    @Column(name = "pepper")
    private String pepper;

    @Column(name = "oregano")
    private String oregano;
    //endregion

    //region Getters & Setters

    public String getMeat() {
        return meat;
    }

    public void setMeat(String meat) {
        this.meat = meat;
    }

    public String getSausage() {
        return sausage;
    }

    public void setSausage(String sausage) {
        this.sausage = sausage;
    }

    public String getOlives() {
        return olives;
    }

    public void setOlives(String olives) {
        this.olives = olives;
    }

    public String getPasta() {
        return pasta;
    }

    public void setPasta(String pasta) {
        this.pasta = pasta;
    }

    public String getGarlic() {
        return garlic;
    }

    public void setGarlic(String garlic) {
        this.garlic = garlic;
    }

    public String getPepper() {
        return pepper;
    }

    public void setPepper(String pepper) {
        this.pepper = pepper;
    }

    public String getOregano() {
        return oregano;
    }

    public void setOregano(String oregano) {
        this.oregano = oregano;
    }

    //endregion
}
