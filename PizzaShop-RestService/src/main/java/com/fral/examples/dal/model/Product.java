package com.fral.examples.dal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "product")
public abstract class Product implements Serializable {

    //region Fields
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "product_id", nullable = false, unique = true)
    private Long id;

    @Column(name = "price")
    private Double price;

    @Column(name = "detail")
    private String detail;

    @Column(name = "name")
    private String name;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "time_to_be_prepared")
    private Long timeToBePrepared;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    private Order order;
    //endregion

    //region Getters & Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Long getTimeToBePrepared() {
        return timeToBePrepared;
    }

    public void setTimeToBePrepared(Long timeToBePrepared) {
        this.timeToBePrepared = timeToBePrepared;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    //endregion
}
