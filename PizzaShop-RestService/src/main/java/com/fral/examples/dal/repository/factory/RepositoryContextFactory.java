package com.fral.examples.dal.repository.factory;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Component
public class RepositoryContextFactory implements ApplicationContextAware {

    private static ApplicationContext context = null;

    public <T extends JpaRepository> T createInstance(Class<T> cmdClazz) {
        return context.getBean(cmdClazz);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
