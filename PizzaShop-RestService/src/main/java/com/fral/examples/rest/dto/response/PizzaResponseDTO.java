package com.fral.examples.rest.dto.response;

import com.fral.examples.common.utils.PizzaType;
import com.fral.examples.dal.model.ExtraIngredient;
import com.fral.examples.dal.model.Pizza;
import com.fral.examples.dal.model.Product;

import java.util.HashSet;
import java.util.Set;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class PizzaResponseDTO extends BaseResponseDTO {

    private String cheese;
    private String sauce;
    private String crust;
    private int slides;
    private PizzaType pizzaType;
    private Set<ExtraIngredient> customizedIngredients;


    public String getCheese() {
        return cheese;
    }

    public void setCheese(String cheese) {
        this.cheese = cheese;
    }

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public String getCrust() {
        return crust;
    }

    public void setCrust(String crust) {
        this.crust = crust;
    }

    public int getSlides() {
        return slides;
    }

    public void setSlides(int slides) {
        this.slides = slides;
    }

    public PizzaType getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(PizzaType pizzaType) {
        this.pizzaType = pizzaType;
    }

    public Set<ExtraIngredient> getCustomizedIngredients() {
        return customizedIngredients;
    }

    public void setCustomizedIngredients(Set<ExtraIngredient> customizedIngredients) {
        this.customizedIngredients = customizedIngredients;
    }

    @Override
    public <K extends Product> void buildDTOFrom(K entity) {
        super.buildDTOFrom(entity);

        Pizza pizza = Pizza.class.cast(entity);
        if (null != pizza) {
            this.cheese = pizza.getCheese();
            this.sauce = pizza.getSauce();
            this.crust = pizza.getCrust();
            this.slides = pizza.getSlides();
            this.pizzaType = pizza.getPizzaType();

            this.customizedIngredients = new HashSet<>();
            if (null != pizza.getCustomizedIngredients()) {
                this.customizedIngredients.addAll(pizza.getCustomizedIngredients());
            }
        }
    }
}
