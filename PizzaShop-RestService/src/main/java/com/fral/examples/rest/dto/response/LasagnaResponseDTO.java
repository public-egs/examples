package com.fral.examples.rest.dto.response;

import com.fral.examples.dal.model.Lasagna;
import com.fral.examples.dal.model.Product;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class LasagnaResponseDTO extends BaseResponseDTO {

    private String meat;
    private String sausage;
    private String olives;
    private String pasta;
    private String garlic;
    private String pepper;
    private String oregano;


    public String getMeat() {
        return meat;
    }

    public void setMeat(String meat) {
        this.meat = meat;
    }

    public String getSausage() {
        return sausage;
    }

    public void setSausage(String sausage) {
        this.sausage = sausage;
    }

    public String getOlives() {
        return olives;
    }

    public void setOlives(String olives) {
        this.olives = olives;
    }

    public String getPasta() {
        return pasta;
    }

    public void setPasta(String pasta) {
        this.pasta = pasta;
    }

    public String getGarlic() {
        return garlic;
    }

    public void setGarlic(String garlic) {
        this.garlic = garlic;
    }

    public String getPepper() {
        return pepper;
    }

    public void setPepper(String pepper) {
        this.pepper = pepper;
    }

    public String getOregano() {
        return oregano;
    }

    public void setOregano(String oregano) {
        this.oregano = oregano;
    }

    @Override
    public <K extends Product> void buildDTOFrom(K entity) {
        super.buildDTOFrom(entity);

        Lasagna lasagna = Lasagna.class.cast(entity);
        if (null != lasagna) {
            this.meat = lasagna.getMeat();
            this.sausage = lasagna.getSausage();
            this.olives = lasagna.getOlives();
            this.pasta = lasagna.getPasta();
            this.garlic = lasagna.getGarlic();
            this.pepper = lasagna.getPepper();
            this.oregano = lasagna.getOregano();
        }
    }
}
