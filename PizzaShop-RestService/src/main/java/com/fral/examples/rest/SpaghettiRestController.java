package com.fral.examples.rest;

import com.fral.examples.common.factory.ResponseBuilder;
import com.fral.examples.common.response.Response;
import com.fral.examples.dal.model.Product;
import com.fral.examples.dal.model.Spaghetti;
import com.fral.examples.exception.InvalidRequestBodyException;
import com.fral.examples.rest.dto.request.ProductOrderRequestDTO;
import com.fral.examples.rest.dto.response.SpaghettiResponseDTO;
import com.fral.examples.services.api.ProductService;
import com.fral.examples.services.builder.ProductBuilder;
import com.fral.examples.services.builder.SpaghettiBuilder;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Api(
        tags = { "spaghetti-rest-controller" },
        description = "Operations over Spaghetti orders"
)
@RequestMapping(value = "/spaghetti")
@RestController
public class SpaghettiRestController {

    @Autowired
    @Qualifier("spaghettiService")
    private ProductService spaghettiService;

    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<Response<SpaghettiResponseDTO>> orderSpaghetti(@Valid @RequestBody ProductOrderRequestDTO productRequest) throws Exception {

        if (null == productRequest) {
            throw new InvalidRequestBodyException(ProductOrderRequestDTO.class);
        }

        Product spaghetti = spaghettiService.takeTheOrder(productRequest.getOrderId(), productRequest.getAmount());

        SpaghettiResponseDTO spaghettiResponse = null;
        if (null != spaghetti) {
            spaghettiResponse = new SpaghettiResponseDTO();
            spaghettiResponse.buildDTOFrom(spaghetti);
        }

        return ResponseEntity.ok(ResponseBuilder.getInstance()
                                 .setMessage("Create Spaghetti order operation has been executed successfully.")
                                 .build(spaghettiResponse));
    }

    @RequestMapping(
            value = "/{spaghettiId}",
            method = RequestMethod.GET
    )
    public ResponseEntity<Response<SpaghettiResponseDTO>> findSpaghetti(@PathVariable("spaghettiId") Long spaghettiId) {

        Product spaghetti = spaghettiService.findById(spaghettiId);

        SpaghettiResponseDTO spaghettiResponse = null;
        if (null != spaghetti) {
            spaghettiResponse = new SpaghettiResponseDTO();
            spaghettiResponse.buildDTOFrom(spaghetti);
        }

        return ResponseEntity.ok(ResponseBuilder.getInstance()
                                 .setMessage("Find Spaghetti operation has been executed successfully.")
                                 .build(spaghettiResponse));
    }
}
