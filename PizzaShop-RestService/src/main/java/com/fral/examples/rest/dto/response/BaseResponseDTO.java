package com.fral.examples.rest.dto.response;

import com.fral.examples.dal.model.Product;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class BaseResponseDTO {

    private Long id;
    private Double price;
    private String detail;
    private String name;
    private Integer amount;
    private Long timeToBePrepared;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Long getTimeToBePrepared() {
        return timeToBePrepared;
    }

    public void setTimeToBePrepared(Long timeToBePrepared) {
        this.timeToBePrepared = timeToBePrepared;
    }

    public <K extends Product> void buildDTOFrom(K entity) {

        this.id = entity.getId();
        this.price = entity.getPrice();
        this.detail = entity.getDetail();
        this.name = entity.getName();
        this.amount = entity.getAmount();
        this.timeToBePrepared = entity.getTimeToBePrepared();
    }
}
