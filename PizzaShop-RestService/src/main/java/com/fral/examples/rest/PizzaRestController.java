package com.fral.examples.rest;

import com.fral.examples.common.factory.ResponseBuilder;
import com.fral.examples.common.response.Response;
import com.fral.examples.dal.model.ExtraIngredient;
import com.fral.examples.common.utils.PizzaType;
import com.fral.examples.dal.model.Product;
import com.fral.examples.rest.dto.request.ExtraIngredientRequestDTO;
import com.fral.examples.rest.dto.request.PizzaOrderRequestDTO;
import com.fral.examples.rest.dto.response.PizzaResponseDTO;
import com.fral.examples.services.api.ProductService;
import com.fral.examples.services.impl.PizzaServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Api(
        tags = { "pizza-rest-controller" },
        description = "Operations over Pizza orders"
)
@RequestMapping(value = "/pizzas")
@RestController
public class PizzaRestController {

    @Autowired
    @Qualifier("pizzaService")
    private ProductService pizzaService;

    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<Response<PizzaResponseDTO>> orderPizza(@RequestParam("type")PizzaType type,
                                                        @Valid @RequestBody PizzaOrderRequestDTO requestBody) throws Exception {

        Set<ExtraIngredient> otherIngredients = new HashSet<>();

        if (null != requestBody.getExtraIngredients()) {
            for (ExtraIngredientRequestDTO extra : requestBody.getExtraIngredients()) {
                ExtraIngredient otherIngredient = new ExtraIngredient();
                otherIngredient.setExtraTopping(extra.getExtraTopping());

                otherIngredients.add(otherIngredient);
            }
        }

        ((PizzaServiceImpl)pizzaService).setType(type);
        ((PizzaServiceImpl)pizzaService).setSlides(requestBody.getNumberOfSlides());
        ((PizzaServiceImpl)pizzaService).setOtherIngredients(otherIngredients);

        Product pizza = pizzaService.takeTheOrder(requestBody.getOrderId(), requestBody.getAmount());

        PizzaResponseDTO pizzaResponse = null;
        if (null != pizza) {
            pizzaResponse = new PizzaResponseDTO();
            pizzaResponse.buildDTOFrom(pizza);
        }

        return ResponseEntity.ok(ResponseBuilder.getInstance()
                                 .setMessage("Create Pizza order operation has been executed successfully.")
                                 .build(pizzaResponse));
    }

    @RequestMapping(
            value = "/{pizzaId}",
            method = RequestMethod.GET
    )
    public ResponseEntity<Response<PizzaResponseDTO>> findPizza(@PathVariable("pizzaId") Long pizzaId) {

        Product pizza = pizzaService.findById(pizzaId);

        PizzaResponseDTO pizzaResponse = null;
        if (null != pizza) {
            pizzaResponse = new PizzaResponseDTO();
            pizzaResponse.buildDTOFrom(pizza);
        }

        return ResponseEntity.ok(ResponseBuilder.getInstance()
                                 .setMessage("Find Pizza operation has been executed successfully.")
                                 .build(pizzaResponse));
    }
}
