package com.fral.examples.rest;

import com.fral.examples.common.factory.ResponseBuilder;
import com.fral.examples.common.response.Response;
import com.fral.examples.dal.model.Product;
import com.fral.examples.exception.InvalidRequestBodyException;
import com.fral.examples.rest.dto.request.ProductOrderRequestDTO;
import com.fral.examples.rest.dto.response.LasagnaResponseDTO;
import com.fral.examples.services.api.ProductService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Api(
        tags = { "lasagna-rest-controller" },
        description = "Operations over Lasagna orders"
)
@RequestMapping(value = "/lasagnas")
@RestController
public class LasagnaRestController {

    @Autowired
    @Qualifier("lasagnaService")
    private ProductService lasagnaServiceImpl;

    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<Response<LasagnaResponseDTO>> orderLasagna(@Valid @RequestBody ProductOrderRequestDTO productRequest) throws Exception {

        if (null == productRequest) {
            throw new InvalidRequestBodyException(ProductOrderRequestDTO.class);
        }

        Product lasagna = lasagnaServiceImpl.takeTheOrder(productRequest.getOrderId(), productRequest.getAmount());

        LasagnaResponseDTO lasagnaResponse = null;
        if (null != lasagna) {
            lasagnaResponse = new LasagnaResponseDTO();
            lasagnaResponse.buildDTOFrom(lasagna);
        }

        return ResponseEntity.ok(ResponseBuilder.getInstance()
                                 .setMessage("Create Lasagna order operation has been executed successfully.")
                                 .build(lasagnaResponse));
    }

    @RequestMapping(
            value = "/{lasagnaId}",
            method = RequestMethod.GET
    )
    public ResponseEntity<Response<LasagnaResponseDTO>> getLasagnasByOrder(@PathVariable("lasagnaId") Long lasagnaId) {

        Product lasagna = lasagnaServiceImpl.findById(lasagnaId);

        LasagnaResponseDTO lasagnaResponse = null;
        if (null != lasagna) {
            lasagnaResponse = new LasagnaResponseDTO();
            lasagnaResponse.buildDTOFrom(lasagna);
        }

        return ResponseEntity.ok(ResponseBuilder.getInstance()
                .setMessage("Get Lasagnas by Order ID has been executed successfully.")
                .build(lasagnaResponse));
    }
}
