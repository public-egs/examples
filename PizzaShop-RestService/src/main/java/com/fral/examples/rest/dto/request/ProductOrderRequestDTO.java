package com.fral.examples.rest.dto.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class ProductOrderRequestDTO {

    @NotNull
    @Min(1)
    private Long orderId;

    @NotNull
    @Min(1)
    private Integer amount;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
