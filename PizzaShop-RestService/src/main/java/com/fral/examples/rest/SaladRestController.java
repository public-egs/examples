package com.fral.examples.rest;

import com.fral.examples.common.factory.ResponseBuilder;
import com.fral.examples.common.response.Response;
import com.fral.examples.dal.model.Product;
import com.fral.examples.exception.InvalidRequestBodyException;
import com.fral.examples.rest.dto.request.ProductOrderRequestDTO;
import com.fral.examples.rest.dto.response.SaladResponseDTO;
import com.fral.examples.services.api.ProductService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Api(
        tags = { "salad-rest-controller" },
        description = "Operations over Salad orders"
)
@RequestMapping(value = "/salads")
@RestController
public class SaladRestController {

    @Autowired
    @Qualifier("saladService")
    private ProductService saladService;


    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<Response<SaladResponseDTO>> orderSalad(@Valid @RequestBody ProductOrderRequestDTO productRequest) throws Exception {

        if (null == productRequest) {
            throw new InvalidRequestBodyException(ProductOrderRequestDTO.class);
        }

        Product salad = saladService.takeTheOrder(productRequest.getOrderId(), productRequest.getAmount());

        SaladResponseDTO saladResponse = null;
        if (null != salad) {
            saladResponse = new SaladResponseDTO();
            saladResponse.buildDTOFrom(salad);
        }

        return ResponseEntity.ok(ResponseBuilder.getInstance()
                                 .setMessage("Create Salad order operation has been executed successfully.")
                                 .build(saladResponse));
    }

    @RequestMapping(
            value = "/{saladId}",
            method = RequestMethod.GET
    )
    public ResponseEntity<Response<SaladResponseDTO>> findSalad(@PathVariable("saladId") Long saladId) {

        Product salad = saladService.findById(saladId);

        SaladResponseDTO saladResponse = null;
        if (null != salad) {
            saladResponse = new SaladResponseDTO();
            saladResponse.buildDTOFrom(salad);
        }

        return ResponseEntity.ok(ResponseBuilder.getInstance()
                                 .setMessage("Find Salad operation has been executed successfully.")
                                 .build(saladResponse));
    }
}
