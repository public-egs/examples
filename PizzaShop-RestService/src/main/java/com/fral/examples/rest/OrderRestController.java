package com.fral.examples.rest;

import com.fral.examples.common.factory.ResponseBuilder;
import com.fral.examples.common.factory.ResponsesBuilder;
import com.fral.examples.common.response.Response;
import com.fral.examples.common.response.Responses;
import com.fral.examples.common.utils.OrderStatus;
import com.fral.examples.dal.model.Order;
import com.fral.examples.exception.OrderNotFoundException;
import com.fral.examples.rest.dto.response.OrderResponseDTO;
import com.fral.examples.services.api.OrderService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.LinkedList;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Api(
        tags = { "order-rest-controller" },
        description = "Operations over Orders"
)
@RequestMapping(value = "/orders")
@RestController
public class OrderRestController {

    @Autowired
    private OrderService orderService;


    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<Response<OrderResponseDTO>> createOrder() {

        Order response = orderService.createOrder();

        OrderResponseDTO orderResponse = null;
        if (null != response) {
            orderResponse = new OrderResponseDTO();
            orderResponse.buildDTOFrom(response);
        }

        return ResponseEntity.ok(ResponseBuilder.getInstance()
                                 .setMessage("Create Order operation has been executed successfully.")
                                 .build(orderResponse));
    }

    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity<Responses<OrderResponseDTO>> getOrders(@RequestParam("status")OrderStatus status) {
        Collection<Order> orders = orderService.getOrders(status);

        Collection<OrderResponseDTO> ordersResult = new LinkedList<>();
        if (null != orders && orders.size() > 0) {
            for (Order item : orders) {
                OrderResponseDTO orderDto = new OrderResponseDTO();
                orderDto.buildDTOFrom(item);

                ordersResult.add(orderDto);
            }
        }

        return ResponseEntity.ok(ResponsesBuilder.getInstance()
                                 .setMessage("Gets Orders by Status operation has been executed successfully.")
                                 .build(ordersResult));
    }

    @RequestMapping(
            value = "/{orderId}",
            method = RequestMethod.GET
    )
    public ResponseEntity<Response<OrderResponseDTO>> findOrderById(@PathVariable("orderId") Long orderId) {
        Order order = orderService.findById(orderId);

        if (null == order) {
            throw new OrderNotFoundException(orderId);
        }

        OrderResponseDTO orderResponse = new OrderResponseDTO();
        orderResponse.buildDTOFrom(order);

        return ResponseEntity.ok(ResponseBuilder.getInstance()
                                 .setMessage("Find Order by ID operation has been executed successfully.")
                                 .build(orderResponse));
    }



}
