package com.fral.examples.rest.dto.response;

import com.fral.examples.dal.model.Product;
import com.fral.examples.dal.model.Spaghetti;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class SpaghettiResponseDTO extends BaseResponseDTO {


    private String meat;
    private String jitomates;
    private String puree;
    private String pasta;
    private String oil;


    public String getMeat() {
        return meat;
    }

    public void setMeat(String meat) {
        this.meat = meat;
    }

    public String getJitomates() {
        return jitomates;
    }

    public void setJitomates(String jitomates) {
        this.jitomates = jitomates;
    }

    public String getPuree() {
        return puree;
    }

    public void setPuree(String puree) {
        this.puree = puree;
    }

    public String getPasta() {
        return pasta;
    }

    public void setPasta(String pasta) {
        this.pasta = pasta;
    }

    public String getOil() {
        return oil;
    }

    public void setOil(String oil) {
        this.oil = oil;
    }


    @Override
    public <K extends Product> void buildDTOFrom(K entity) {
        super.buildDTOFrom(entity);

        Spaghetti spaghetti = Spaghetti.class.cast(entity);
        if (null != spaghetti) {
            this.meat = spaghetti.getMeat();
            this.jitomates = spaghetti.getJitomates();
            this.puree = spaghetti.getPuree();
            this.pasta = spaghetti.getPasta();
            this.oil = spaghetti.getOil();
        }
    }
}
