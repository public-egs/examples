package com.fral.examples.rest.dto.response;

import com.fral.examples.dal.model.Product;
import com.fral.examples.dal.model.Salad;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class SaladResponseDTO extends BaseResponseDTO {


    private String quinoa;
    private String oil;
    private String vinegar;
    private String salt;
    private String onion;


    public String getQuinoa() {
        return quinoa;
    }

    public void setQuinoa(String quinoa) {
        this.quinoa = quinoa;
    }

    public String getOil() {
        return oil;
    }

    public void setOil(String oil) {
        this.oil = oil;
    }

    public String getVinegar() {
        return vinegar;
    }

    public void setVinegar(String vinegar) {
        this.vinegar = vinegar;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getOnion() {
        return onion;
    }

    public void setOnion(String onion) {
        this.onion = onion;
    }


    @Override
    public <K extends Product> void buildDTOFrom(K entity) {
        super.buildDTOFrom(entity);

        Salad salad = Salad.class.cast(entity);
        if (null != entity) {
            this.quinoa = salad.getQuinoa();
            this.oil = salad.getOil();
            this.vinegar = salad.getVinegar();
            this.salt = salad.getSalt();
            this.onion = salad.getOnion();
        }
    }
}
