package com.fral.examples.rest.dto.response;

import com.fral.examples.common.utils.OrderStatus;
import com.fral.examples.dal.model.Order;

import java.util.Date;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class OrderResponseDTO {

    private Long id;
    private String orderDetail;
    private OrderStatus status;
    private Date orderDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(String orderDetail) {
        this.orderDetail = orderDetail;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public <K extends Order> void buildDTOFrom(K entity) {

        this.id = entity.getId();
        this.orderDetail = entity.getOrderDetail();
        this.status = entity.getStatus();
        this.orderDate = entity.getOrderDate();
    }
}
