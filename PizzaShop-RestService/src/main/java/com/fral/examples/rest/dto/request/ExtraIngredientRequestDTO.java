package com.fral.examples.rest.dto.request;

import javax.validation.constraints.NotNull;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class ExtraIngredientRequestDTO {

    @NotNull
    private String extraTopping;

    public String getExtraTopping() {
        return extraTopping;
    }

    public void setExtraTopping(String extraTopping) {
        this.extraTopping = extraTopping;
    }
}
