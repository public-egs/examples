package com.fral.examples.rest.dto.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class PizzaOrderRequestDTO extends ProductOrderRequestDTO {

    @NotNull
    @Min(1)
    private Integer numberOfSlides;

    private Set<ExtraIngredientRequestDTO> extraIngredients;


    public Integer getNumberOfSlides() {
        return numberOfSlides;
    }

    public void setNumberOfSlides(Integer numberOfSlides) {
        this.numberOfSlides = numberOfSlides;
    }

    public Set<ExtraIngredientRequestDTO> getExtraIngredients() {
        return extraIngredients;
    }

    public void setExtraIngredients(Set<ExtraIngredientRequestDTO> extraIngredients) {
        this.extraIngredients = extraIngredients;
    }
}
