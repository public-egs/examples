package com.fral.examples.services.builder;

import com.fral.examples.common.utils.ProductType;
import com.fral.examples.dal.model.Salad;
import org.springframework.stereotype.Component;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Component("saladBuilder")
public class SaladBuilder extends ProductBuilder {

    public final double DEFAULT_SALAD_PRICE = 40;

    @Override
    protected void prepareProduct(Integer amount) {
        product = new Salad();
        product.setName("Salad");
        product.setTimeToBePrepared(ProductType.SALAD.getTimeToBePrepared());
        product.setPrice(DEFAULT_SALAD_PRICE);
        product.setAmount(amount);

        ((Salad)product).setOil("Salad Oil");
        ((Salad)product).setOnion("Salad Onion");
        ((Salad)product).setQuinoa("Salad Quinoa");
        ((Salad)product).setSalt("Salad salt");
        ((Salad)product).setVinegar("Salad Vinegar");

        product.setDetail(buildProductDetails());
    }
}
