package com.fral.examples.services.api;

import com.fral.examples.common.utils.OrderStatus;
import com.fral.examples.dal.model.Order;

import java.util.Collection;

public interface OrderService {

    Order createOrder();
    Collection<Order> getOrders(OrderStatus status);
    Order findById(Long orderId);
}
