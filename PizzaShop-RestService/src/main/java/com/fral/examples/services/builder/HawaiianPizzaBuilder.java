package com.fral.examples.services.builder;

import com.fral.examples.dal.model.Pizza;
import com.fral.examples.common.utils.PizzaType;
import org.springframework.stereotype.Component;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Component("hawaiianPizzaBuilder")
public class HawaiianPizzaBuilder extends PizzaBuilder {

    @Override
    protected void prepareProduct(Integer amount) {
        super.prepareProduct(amount);
        product.setName("Hawaiian Pizza");
        ((Pizza)product).setPizzaType(PizzaType.HAWAIIAN);
    }

    @Override
    public void buildCheese() {
        ((Pizza)product).setCheese("Hawaiian Cheese");
    }

    @Override
    public void buildSauce() {
        ((Pizza)product).setSauce("Hawaiian sauce");
    }

    @Override
    public void buildCrust() {
        ((Pizza)product).setCrust("Hawaiian crust");
    }
}
