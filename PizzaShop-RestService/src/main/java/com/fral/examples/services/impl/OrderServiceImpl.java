package com.fral.examples.services.impl;

import com.fral.examples.common.utils.OrderStatus;
import com.fral.examples.dal.model.Order;
import com.fral.examples.dal.repository.api.OrderRepository;
import com.fral.examples.services.api.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Collection;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository repository;


    @Override
    public Order createOrder() {

        Order newOrder = new Order();
        newOrder.setOrderDetail("");
        newOrder.setStatus(OrderStatus.PENDING);
        newOrder.setOrderDate(Calendar.getInstance().getTime());

        return repository.save(newOrder);
    }

    @Override
    public Collection<Order> getOrders(OrderStatus status) {

        Collection<Order> ordersResponse;
        if (status == OrderStatus.ALL) {
            ordersResponse = repository.findAll();
        } else {
            ordersResponse = repository.findByStatus(status);
        }

        return ordersResponse;
    }

    @Override
    public Order findById(Long orderId) {
        return repository.findOne(orderId);
    }
}
