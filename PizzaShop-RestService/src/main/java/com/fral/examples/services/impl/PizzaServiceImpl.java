package com.fral.examples.services.impl;

import com.fral.examples.common.utils.PizzaType;
import com.fral.examples.dal.model.ExtraIngredient;
import com.fral.examples.dal.model.Order;
import com.fral.examples.dal.model.Pizza;
import com.fral.examples.dal.model.Product;
import com.fral.examples.dal.repository.api.OrderRepository;
import com.fral.examples.dal.repository.api.PizzaRepository;
import com.fral.examples.exception.OrderNotFoundException;
import com.fral.examples.exception.UnavailableProductException;
import com.fral.examples.services.api.ProductService;
import com.fral.examples.services.builder.BrazilianPizzaBuilder;
import com.fral.examples.services.builder.HawaiianPizzaBuilder;
import com.fral.examples.services.builder.PizzaBuilder;
import com.fral.examples.services.builder.VeggiePizzaBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * @author franco.robert.fral@gmail.com
 */

@Service("pizzaService")
public class PizzaServiceImpl implements ProductService {

    @Autowired
    private PizzaRepository pizzaRepository;

    @Autowired
    private OrderRepository orderRepository;

    private PizzaType type;

    private int slides;

    private Set<ExtraIngredient> otherIngredients;


    public void setType(PizzaType type) {
        this.type = type;
    }

    public void setSlides(int slides) {
        this.slides = slides;
    }

    public void setOtherIngredients(Set<ExtraIngredient> otherIngredients) {
        this.otherIngredients = otherIngredients;
    }

    @Override
    public Product takeTheOrder(Long orderId, Integer amount) {
        Order existingOrder = orderRepository.findOne(orderId);

        if (null == existingOrder){
            throw new OrderNotFoundException(orderId);
        }

        PizzaBuilder builder = getPizzaBuilder();
        builder.buildProduct(amount);
        builder.setConfigurableProperties(slides, otherIngredients);
        Pizza productToSave = Pizza.class.cast(builder.getProduct());
        productToSave.setOrder(existingOrder);

        return pizzaRepository.save(productToSave);
    }

    @Override
    public Product findById(Long productId) {
        return pizzaRepository.findOne(productId);
    }

    private PizzaBuilder getPizzaBuilder() {

        PizzaBuilder pizzaBuilder;
        switch (type) {
            case VEGGIE:
                pizzaBuilder = new VeggiePizzaBuilder();
                break;
            case HAWAIIAN:
                pizzaBuilder = new HawaiianPizzaBuilder();
                break;
            case BRAZILIAN:
                pizzaBuilder = new BrazilianPizzaBuilder();
                break;
            default:
                throw new UnavailableProductException(type.toString());
        }

        return pizzaBuilder;
    }
}
