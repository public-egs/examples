package com.fral.examples.services.builder;

import com.fral.examples.common.utils.ProductType;
import com.fral.examples.dal.model.Spaghetti;
import org.springframework.stereotype.Component;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Component("spaghettiBuilder")
public class SpaghettiBuilder extends ProductBuilder {

    public final double DEFAULT_SPAGHETTI_PRICE = 120;

    @Override
    protected void prepareProduct(Integer amount) {
        product = new Spaghetti();
        product.setName("Spaghetti");
        product.setTimeToBePrepared(ProductType.SPAGHETTI.getTimeToBePrepared());
        product.setPrice(DEFAULT_SPAGHETTI_PRICE);
        product.setAmount(amount);

        ((Spaghetti)product).setJitomates("Spaghetti Jitomates");
        ((Spaghetti)product).setMeat("Spaghetti Meat");
        ((Spaghetti)product).setOil("Spaghetti Oil");
        ((Spaghetti)product).setPasta("Spaghetti Pasta");
        ((Spaghetti)product).setPuree("Spaghetti Pure");

        product.setDetail(buildProductDetails());
    }
}
