package com.fral.examples.services.builder;

import com.fral.examples.dal.model.Pizza;
import com.fral.examples.common.utils.PizzaType;
import org.springframework.stereotype.Component;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Component("veggiePizzaBuilder")
public class VeggiePizzaBuilder extends PizzaBuilder {

    @Override
    protected void prepareProduct(Integer amount) {
        super.prepareProduct(amount);
        product.setName("Veggie Pizza");
        ((Pizza)product).setPizzaType(PizzaType.VEGGIE);
    }

    @Override
    public void buildCheese() {
        ((Pizza)product).setCheese("Vegetarian Cheese");
    }

    @Override
    public void buildSauce() {
        ((Pizza)product).setSauce("Vegetarian Sauce");
    }

    @Override
    public void buildCrust() {
        ((Pizza)product).setCrust("Vegetarian Crust");
    }
}
