package com.fral.examples.services.builder;

import com.fral.examples.common.utils.ProductType;
import com.fral.examples.dal.model.Lasagna;
import org.springframework.stereotype.Component;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Component("lasagnaBuilder")
public class LasagnaBuilder extends ProductBuilder {

    public final double DEFAULT_LASAGNA_PRICE = 90;

    @Override
    protected void prepareProduct(Integer amount) {
        product = new Lasagna();
        product.setName("Lasagna");
        product.setTimeToBePrepared(ProductType.LASAGNA.getTimeToBePrepared());
        product.setPrice(DEFAULT_LASAGNA_PRICE);
        product.setAmount(amount);

        ((Lasagna)product).setGarlic("Lasagna Garlic");
        ((Lasagna)product).setMeat("Lasagna Meet");
        ((Lasagna)product).setOlives("Lasagna Olive");
        ((Lasagna)product).setOregano("Lasagna Oregano");
        ((Lasagna)product).setPasta("Lasagna Pasta");
        ((Lasagna)product).setPepper("Lasagna Pepper");
        ((Lasagna)product).setSausage("Lasagna Sausage");

        product.setDetail(buildProductDetails());
    }

}
