package com.fral.examples.services.builder;

import com.fral.examples.dal.model.Pizza;
import com.fral.examples.common.utils.PizzaType;
import org.springframework.stereotype.Component;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Component("brazilianPizzaBuilder")
public class BrazilianPizzaBuilder extends PizzaBuilder {

    @Override
    protected void prepareProduct(Integer amount) {
        super.prepareProduct(amount);
        product.setName("Brazilian Pizza");
        ((Pizza)product).setPizzaType(PizzaType.BRAZILIAN);
    }

    @Override
    public void buildCheese() {
        ((Pizza)product).setCheese("Brazilian Cheese");
    }

    @Override
    public void buildSauce() {
        ((Pizza)product).setSauce("Brazilian Sauce");
    }

    @Override
    public void buildCrust() {
        ((Pizza)product).setCrust("Brazilian Crust");
    }
}
