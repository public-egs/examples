package com.fral.examples.services.api;

import com.fral.examples.dal.model.Product;

public interface ProductService {

    Product takeTheOrder(Long orderId, Integer amount);
    Product findById(Long productId);
}
