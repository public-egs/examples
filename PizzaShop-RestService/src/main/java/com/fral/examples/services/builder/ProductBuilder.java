package com.fral.examples.services.builder;

import com.fral.examples.dal.model.Product;

/**
 * @author: franco.robert.fral@gmail.com.
 */

public abstract class ProductBuilder {

    protected final Integer DEFAULT_PRODUCT_ORDER_AMOUNT = 1;
    protected final String PRODUCT_DETAIL_PATTERN = "PRODUCT: %1$s - DESCRIPTION: %2$s - AMOUNT: %3$s - UNIT PRICE: %4$s - TOTAL COST: %5$s";

    protected Product product;

    public Product getProduct() {
        if (null == product) {
            buildProduct(DEFAULT_PRODUCT_ORDER_AMOUNT);
        }

        return product;
    }

    public void buildProduct(Integer amount) {
        prepareProduct(amount);
    }

    protected abstract void prepareProduct(Integer amount);

    protected String buildProductDetails() {
        if (null == product) {
            return "";
        }

        String description = product.getName() + " order has been registered.";
        return String.format(PRODUCT_DETAIL_PATTERN, product.getName(), description, product.getAmount(), product.getPrice(), product.getPrice() * product.getAmount());
    }
}
