package com.fral.examples.services.impl;

import com.fral.examples.dal.model.Lasagna;
import com.fral.examples.dal.model.Order;
import com.fral.examples.dal.model.Product;
import com.fral.examples.dal.repository.api.LasagnaRepository;
import com.fral.examples.dal.repository.api.OrderRepository;
import com.fral.examples.exception.OrderNotFoundException;
import com.fral.examples.services.api.ProductService;
import com.fral.examples.services.builder.ProductBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("lasagnaService")
public class LasagnaServiceImpl implements ProductService {

    @Autowired
    private LasagnaRepository lasagnaRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    @Qualifier("lasagnaBuilder")
    private ProductBuilder lasagnaBuilder;


    @Override
    public Product takeTheOrder(Long orderId, Integer amount) {
        Order existingOrder = orderRepository.findOne(orderId);
        if (null == existingOrder) {
            throw new OrderNotFoundException(orderId);
        }

        lasagnaBuilder.buildProduct(amount);
        Lasagna productToSave = Lasagna.class.cast(lasagnaBuilder.getProduct());
        productToSave.setOrder(existingOrder);

        return lasagnaRepository.save(productToSave);
    }

    @Override
    public Product findById(Long productId) {

        return lasagnaRepository.findOne(productId);
    }
}
