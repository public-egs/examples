package com.fral.examples.services.builder;

import com.fral.examples.common.utils.PizzaSize;
import com.fral.examples.common.utils.ProductType;
import com.fral.examples.dal.model.ExtraIngredient;
import com.fral.examples.dal.model.Pizza;

import java.util.Set;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public abstract class PizzaBuilder extends ProductBuilder {

    protected final Double DEFAULT_PIZZA_PRICE = 40.0;
    protected final Double EXTRA_INGREDIENT_PRICE = 5.0;

    @Override
    protected void prepareProduct(Integer amount) {
        product = new Pizza();
        product.setTimeToBePrepared(ProductType.PIZZA.getTimeToBePrepared());
        product.setAmount(amount);

        buildCheese();
        buildSauce();
        buildCrust();
    }

    public void setConfigurableProperties(Integer slides, Set<ExtraIngredient> otherIngredients) {
        double initialPrice = DEFAULT_PIZZA_PRICE;

        PizzaSize pizzaSize = PizzaSize.parse(slides);

        if (pizzaSize != PizzaSize.CUSTOM) {
            initialPrice = pizzaSize.getPrice();
        }

        if (null != otherIngredients && otherIngredients.size() > 0) {
            product.setPrice(initialPrice + (otherIngredients.size() * EXTRA_INGREDIENT_PRICE));
            ((Pizza)product).setCustomizedIngredients(otherIngredients);
        } else {
            product.setPrice(initialPrice);
        }

        ((Pizza)product).setSlides(slides);
        product.setDetail(buildProductDetails());
    }

    @Override
    protected String buildProductDetails() {
        if (null == product) {
            return "";
        }

        PizzaSize pizzaSize = PizzaSize.parse(((Pizza)product).getSlides());
        String description = "Pizza size: " + pizzaSize + "(" + ((Pizza)product).getSlides() + " slides)";
        return String.format(PRODUCT_DETAIL_PATTERN, product.getName(), description, product.getAmount(), product.getPrice(), product.getPrice() * product.getAmount());
    }

    protected abstract void buildCheese();
    protected abstract void buildSauce();
    protected abstract void buildCrust();

}
