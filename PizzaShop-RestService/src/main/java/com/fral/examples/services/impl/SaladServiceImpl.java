package com.fral.examples.services.impl;

import com.fral.examples.dal.model.Order;
import com.fral.examples.dal.model.Product;
import com.fral.examples.dal.model.Salad;
import com.fral.examples.dal.repository.api.OrderRepository;
import com.fral.examples.dal.repository.api.SaladsRepository;
import com.fral.examples.exception.OrderNotFoundException;
import com.fral.examples.services.api.ProductService;
import com.fral.examples.services.builder.ProductBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("saladService")
public class SaladServiceImpl implements ProductService {

    @Autowired
    private SaladsRepository saladRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    @Qualifier("saladBuilder")
    private ProductBuilder saladBuilder;


    @Override
    public Product takeTheOrder(Long orderId, Integer amount) {
        Order existingOrder = orderRepository.findOne(orderId);
        if (null == existingOrder) {
            throw new OrderNotFoundException(orderId);
        }

        saladBuilder.buildProduct(amount);
        Salad productToSave = Salad.class.cast(saladBuilder.getProduct());
        productToSave.setOrder(existingOrder);

        return saladRepository.save(productToSave);
    }

    @Override
    public Product findById(Long productId) {
        return saladRepository.findOne(productId);
    }
}
