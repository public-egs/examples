package com.fral.examples.services.impl;

import com.fral.examples.dal.model.Order;
import com.fral.examples.dal.model.Product;
import com.fral.examples.dal.model.Spaghetti;
import com.fral.examples.dal.repository.api.OrderRepository;
import com.fral.examples.dal.repository.api.SpaghettiRepository;
import com.fral.examples.exception.OrderNotFoundException;
import com.fral.examples.services.api.ProductService;
import com.fral.examples.services.builder.ProductBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("spaghettiService")
public class SpaghettiServiceImpl implements ProductService {

    @Autowired
    private SpaghettiRepository spaghettiRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    @Qualifier("spaghettiBuilder")
    private ProductBuilder spaghettiBuilder;


    @Override
    public Product takeTheOrder(Long orderId, Integer amount) {
        Order existingOrder = orderRepository.findOne(orderId);
        if (null == existingOrder) {
            throw new OrderNotFoundException(orderId);
        }

        spaghettiBuilder.buildProduct(amount);
        Spaghetti productToSave = Spaghetti.class.cast(spaghettiBuilder.getProduct());
        productToSave.setOrder(existingOrder);

        return spaghettiRepository.save(productToSave);
    }

    @Override
    public Product findById(Long productId) {
        return spaghettiRepository.findOne(productId);
    }
}
