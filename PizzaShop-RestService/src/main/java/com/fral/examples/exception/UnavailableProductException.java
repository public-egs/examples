package com.fral.examples.exception;

/**
 * @author franco.robert.fral@gmail.com
 */
@ExceptionHandlerCfg(code = 400, message = "The requested product type is not available. The operation can not continue.")
public class UnavailableProductException extends PizzaShopServiceException {

    private String productType;

    public UnavailableProductException(String productType) {
        this.productType = productType;
    }

    @Override
    protected void accept(ErrorResponseBuilder builder) {
        builder.addErrorData("productType", productType);
    }
}
