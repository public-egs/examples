package com.fral.examples.exception;

import com.fral.examples.common.factory.ResponseBuilder;
import com.fral.examples.common.response.Response;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author franco.robert.fral@gmail.com
 */
@ControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(PizzaShopServiceException.class)
    public ResponseEntity<Response<ErrorResponse>> handleSocialServiceExceptionException(PizzaShopServiceException exception) {
        ExceptionHandlerCfg handlerCfg = AnnotationUtils.findAnnotation(exception.getClass(), ExceptionHandlerCfg.class);

        if (null == handlerCfg) {
            throw new UnsupportedOperationException(
                    "The annotation @ExceptionHandlerCfg is required in exception class: '"
                            + exception.getClass()
                            + "' "
            );
        }

        if (StringUtils.isEmpty(handlerCfg.message())) {
            throw new UnsupportedOperationException("The message in @ExceptionHandlerCfg annotation cannot be empty");
        }

        ErrorResponseBuilder builder = ErrorResponseBuilder.getInstance(exception.getClass().getSimpleName());
        exception.accept(builder);

        return ResponseEntity.ok(ResponseBuilder.getInstance()
                .setCode(handlerCfg.code())
                .setMessage(handlerCfg.message())
                .build(builder.build()));
    }
}
