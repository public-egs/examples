package com.fral.examples.exception;

/**
 * @author franco.robert.fral@gmail.com
 */
@ExceptionHandlerCfg(code = 400, message = "RequestBody parameter can not be null.")
public class InvalidRequestBodyException extends PizzaShopServiceException {

    private Class requestBodyParam;

    public InvalidRequestBodyException(Class bodyParameterName) {
        this.requestBodyParam = bodyParameterName;
    }


    @Override
    protected void accept(ErrorResponseBuilder builder) {
        builder.addErrorData("productRequestBodyParameter", requestBodyParam);
    }
}
