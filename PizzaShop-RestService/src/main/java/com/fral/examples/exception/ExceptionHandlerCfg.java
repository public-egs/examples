package com.fral.examples.exception;

import java.lang.annotation.*;

/**
 * @author franco.robert.fral@gmail.com
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@interface ExceptionHandlerCfg {

    int code();

    String message();

}
