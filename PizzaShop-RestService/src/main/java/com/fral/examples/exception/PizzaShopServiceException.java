package com.fral.examples.exception;

/**
 * @author franco.robert.fral@gmail.com
 */
public abstract class PizzaShopServiceException extends RuntimeException {

    protected abstract void accept(ErrorResponseBuilder builder);
}
