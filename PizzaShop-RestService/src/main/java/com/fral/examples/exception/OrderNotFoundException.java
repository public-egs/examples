package com.fral.examples.exception;

/**
 * @author franco.robert.fral@gmail.com
 */
@ExceptionHandlerCfg(code = 400, message = "Order could not be found or it does not exists. " +
        "If it does not exist, first you will need to create one because it is required to create products orders.")
public class OrderNotFoundException extends PizzaShopServiceException {

    private Long orderId;

    public OrderNotFoundException(Long orderId) {
        this.orderId = orderId;
    }

    @Override
    protected void accept(ErrorResponseBuilder builder) {
        builder.addErrorData("orderId", orderId);
    }
}
