package com.fral.examples.config;

import com.fral.examples.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author: franco.robert.fral@gmail.com.
 */
@Component
public class CorsFilter implements Filter {

    @Autowired
    private CorsConfig config;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        /* Enable wild card '*' which is not supports by default in web-sockets */
        HttpServletRequest request = (HttpServletRequest) req;
        String allowOrigin = getAllowOrigin();
        if (getAllowOrigin().equals("*") && request.getHeader("origin") != null) {
            allowOrigin = request.getHeader("origin");
        }

        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", allowOrigin);
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, x-requested-with, X-Custom-Header");
        chain.doFilter(req, res);
    }

    @Override
    public void destroy() {

    }

    private String getAllowOrigin() {
        if (StringUtils.getInstance().isBlankOrNull(config.getAllowOrigin())) {
            return "*";
        }

        return config.getAllowOrigin();
    }
}

