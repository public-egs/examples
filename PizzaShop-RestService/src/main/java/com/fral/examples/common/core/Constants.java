package com.fral.examples.common.core;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public final class Constants {

    public static class Code {
        public static final int OK = 200;
        public static final int BAD_REQUEST = 400;
        public static final int INTERNAL_ERROR = 500;
    }

    public static class Success {
        public static final int CODE = 200;
        public static final String CONTENT = "SUCCESS";
    }

    public static class Order {
        public static final String DESC = "DESC";
        public static final String DEFAULT_ORDER_ASC = "ASC";
        public static final int DEFAULT_PAGE = 0;
        public static final int DEFAULT_SIZE = 10;
    }

    public static class ErrorMessage {
        public static final String ERROR_PAGE = "Page index must not be less than zero!";
        public static final String ERROR_SIZE = "Page size must not be less than one!";
        public static final String ERROR_SORT = "You have to provide one property correct. ASC or DESC";
    }

    private Constants() {
    }
}
