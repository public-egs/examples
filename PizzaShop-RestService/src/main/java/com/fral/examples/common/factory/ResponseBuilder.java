package com.fral.examples.common.factory;

import com.fral.examples.common.core.Constants;
import com.fral.examples.common.response.Response;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public final class ResponseBuilder {

    private Integer code;

    private String message;

    public static ResponseBuilder getInstance() {
        return new ResponseBuilder();
    }

    private ResponseBuilder() {
    }

    public ResponseBuilder setCode(Integer code) {
        this.code = code;

        return this;
    }

    public ResponseBuilder setMessage(String message) {
        this.message = message;

        return this;
    }

    public <T> Response<T> build(T content) {
        ResponseFactory<T> factory;

        if (null == code) {
            factory = new ResponseFactory<>(content, Constants.Code.OK);
        } else {
            factory = new ResponseFactory<>(content, code);
        }

        factory.setMessage(message);

        return factory.create();
    }

    public Response<String> buildSuccess() {
        ResponseFactory<String> factory = new ResponseFactory<>(
                Constants.Success.CONTENT,
                Constants.Success.CODE
        );

        factory.setMessage(message);

        return factory.create();
    }
}
