package com.fral.examples.common.response;

import java.util.Collection;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class Responses<T> extends AbstractResponse<ResponsesMetadata> {

    private Collection<T> content;

    public Collection<T> getContent() {
        return content;
    }

    public void setContent(Collection<T> content) {
        this.content = content;
    }
}
