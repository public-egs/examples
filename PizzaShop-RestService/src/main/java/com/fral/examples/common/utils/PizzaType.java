package com.fral.examples.common.utils;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public enum PizzaType {

    HAWAIIAN,
    BRAZILIAN,
    VEGGIE
}
