package com.fral.examples.common.factory;

import com.fral.examples.common.core.Metadata;
import com.fral.examples.common.response.AbstractResponse;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public abstract class AbstractResponseFactory<U extends AbstractResponse<V>, V extends Metadata> {

    public U create() {
        U instance = createInstance();
        V metadataInstance = createMetadataInstance();

        instance.setMetadata(metadataInstance);

        return instance;
    }

    protected abstract U createInstance();

    protected abstract V createMetadataInstance();
}
