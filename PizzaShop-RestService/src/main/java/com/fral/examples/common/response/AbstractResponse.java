package com.fral.examples.common.response;

import com.fral.examples.common.core.Metadata;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public abstract class AbstractResponse<U extends Metadata> {

    private U metadata;

    public U getMetadata() {
        return metadata;
    }

    public void setMetadata(U metadata) {
        this.metadata = metadata;
    }
}
