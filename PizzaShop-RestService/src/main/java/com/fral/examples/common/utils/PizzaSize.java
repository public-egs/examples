package com.fral.examples.common.utils;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public enum PizzaSize {
    PERSONAL(2, 10),
    SMALL(4, 15),
    MEDIUM(8, 25),
    LARGE(16, 40),
    CUSTOM(-1, -1);


    private int slides;
    private double price;

    PizzaSize(int slides, double price) {
        this.slides = slides;
        this.price = price;
    }

    public int getSlides() {
        return slides;
    }

    public double getPrice() {
        return price;
    }

    public static PizzaSize parse(int slides) {
        PizzaSize size = PizzaSize.CUSTOM; //Default

        for (PizzaSize item : PizzaSize.values()) {
            if (item.getSlides() == slides) {
                size = item;
                break;
            }
        }

        return size;
    }
}

