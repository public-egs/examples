package com.fral.examples.common.utils;

/**
 * @author franco.robert.fral@gmail.com
 */
public enum OrderStatus {
    PENDING,
    DELIVERED,
    ALL
}
