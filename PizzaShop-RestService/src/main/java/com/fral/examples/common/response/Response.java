package com.fral.examples.common.response;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class Response<T> extends AbstractResponse<ResponseMetadata> {

    private T content;

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
