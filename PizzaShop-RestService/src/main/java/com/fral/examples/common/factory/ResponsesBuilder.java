package com.fral.examples.common.factory;

import com.fral.examples.common.core.Constants;
import com.fral.examples.common.response.Responses;

import java.util.Collection;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public class ResponsesBuilder {

    private Integer code;

    private String message;

    public static ResponsesBuilder getInstance() {
        return new ResponsesBuilder();
    }

    private ResponsesBuilder() {
    }

    public ResponsesBuilder setCode(Integer code) {
        this.code = code;

        return this;
    }

    public ResponsesBuilder setMessage(String message) {
        this.message = message;

        return this;
    }

    public <T> Responses<T> build(Collection<T> content) {
        ResponsesFactory<T> factory;

        if (null == code) {
            factory = new ResponsesFactory<>(content, Constants.Code.OK);
        } else {
            factory = new ResponsesFactory<>(content, code);
        }

        factory.setMessage(message);

        return factory.create();
    }
}
