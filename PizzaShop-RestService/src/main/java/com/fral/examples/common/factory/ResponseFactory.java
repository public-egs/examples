package com.fral.examples.common.factory;

import com.fral.examples.common.response.Response;
import com.fral.examples.common.response.ResponseMetadata;
import com.fral.examples.common.utils.StringUtils;

/**
 * @author: franco.robert.fral@gmail.com.
 */
final class ResponseFactory<T> extends AbstractResponseFactory<Response<T>, ResponseMetadata> {

    private T content;

    private Integer code;

    private String message;

    public ResponseFactory(T content, Integer code) {
        this.content = content;
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    protected Response<T> createInstance() {
        Response<T> result = new Response<>();
        result.setContent(content);

        return result;
    }

    @Override
    protected ResponseMetadata createMetadataInstance() {
        ResponseMetadata result = new ResponseMetadata();
        result.setCode(code);

        String message = "";
        if (!StringUtils.getInstance().isBlankOrNull(this.message)) {
            message = this.message;
        }
        result.setMessage(message);

        return result;
    }
}
