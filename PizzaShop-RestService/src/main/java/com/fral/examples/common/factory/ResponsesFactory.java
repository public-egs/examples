package com.fral.examples.common.factory;

import com.fral.examples.common.response.Responses;
import com.fral.examples.common.response.ResponsesMetadata;
import com.fral.examples.common.utils.StringUtils;

import java.util.Collection;

/**
 * @author: franco.robert.fral@gmail.com.
 */
final class ResponsesFactory<T> extends AbstractResponseFactory<Responses<T>, ResponsesMetadata> {

    private Collection<T> content;

    private Integer code;

    private String message;

    public ResponsesFactory(Collection<T> content, Integer code) {
        this.content = content;
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    protected Responses<T> createInstance() {
        Responses<T> result = new Responses<>();
        result.setContent(content);

        return result;
    }

    @Override
    protected ResponsesMetadata createMetadataInstance() {
        ResponsesMetadata result = new ResponsesMetadata();
        result.setCode(code);

        String message = "";
        if (!StringUtils.getInstance().isBlankOrNull(this.message)) {
            message = this.message;
        }
        result.setMessage(message);

        return result;
    }
}
