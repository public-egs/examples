package com.fral.examples.common.core;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public abstract class AbstractMetadata implements Metadata {

    private Integer code;

    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
