package com.fral.examples.common.utils;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public final class StringUtils {

    public static StringUtils getInstance() {
        return LazyHolder.INSTANCE;
    }

    private StringUtils() {
    }

    public boolean isBlankOrNull(String instance) {
        return null == instance || "".equals(instance.trim());
    }

    private static class LazyHolder {
        static final StringUtils INSTANCE = new StringUtils();
    }
}
