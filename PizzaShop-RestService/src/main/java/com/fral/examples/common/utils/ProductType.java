package com.fral.examples.common.utils;

/**
 * @author: franco.robert.fral@gmail.com.
 */
public enum ProductType {
    PIZZA(30),
    LASAGNA(40),
    SPAGHETTI(60),
    SALAD(15);

    private long timeToBePrepared;

    ProductType(long time) {
        this.timeToBePrepared = time;
    }

    public long getTimeToBePrepared() {
        return timeToBePrepared;
    }
}
