# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Learn about spring boot and some design patterns applied on this project.
* Version 1.0
* [Review more about Pizza-Shop service on its WIKI page](https://bitbucket.org/public-egs/examples/wiki/Home)

### How do I get set up? ###


For build the artifact you need execute the console commands:

+ __Execute the following command to remove the target directory with all the build data before starting so that it is fresh__: 
```
mvn clean
```

+ __Execute the following command to compile your application sources__: 
```
mvn compile
```


+ __Execute the following command to make a JAR file of project__: 
```
mvn package
```



+ __To initialize the service you will need to run the following command.__: 
```
java -jar target\pizza-shop-rest-api-1.0-SNAPSHOT.jar
```

+ __You need to take into account the following configuration in application.yml configuration file__: 
```
spring:
  application:
    id: pizza-shop
  datasource:
    url: jdbc:h2:mem:pizzashop;DB_CLOSE_ON_EXIT=FALSE;DB_CLOSE_DELAY=-1
  h2:
    console:
      enabled: true
      path: /h2-console

server:
  port: 9999
  context-path: /api/v1/pizza-shop

cors:
  allowOrigin: '*'
```

Based on the information that we can see in the configuration file displayed above, the points that we need to take into account in the following order.

**port: 9999** - This means the service is going to run in port *9999* 
Exaple:

```
#!URL

http://localhost:9999/
```
**context-path: /api/v1/pizza-shop** - It is the main path for the service. So endpoing are going to follow this path as follows. 
Example: 


```
#!URL

http://localhost:9999/api/v1/pizza-shot/{defined-endpoint-here}
```

**h2: console: path: /h2-console** -  Pay atention on this, this is going to help you to see information related to the database of the service. 
Example 


```
#!URL

-- http://{server}:{port}/{base-path}/{defined-endpoint-here}

-- http://localhost:9999/api/v1/pizza-shot/h2-console
```

Following the steps mentioned above you are going to have something as follows (the information to access are mentioned in the **application.yml** configuration file).

![h2console.PNG](https://bitbucket.org/repo/XX5MApX/images/4192835597-h2console.PNG)

![h2console-mainwindow.PNG](https://bitbucket.org/repo/XX5MApX/images/906752280-h2console-mainwindow.PNG)


+ __To access the REST Api documentation you will need to go the following path__: 
```
http://localhost:9999/api/v1/pizza-shot/swagger-ui.html
```

After enter the link previously mentioned you will get the following REST Api documentation. 

![swagger.PNG](https://bitbucket.org/repo/XX5MApX/images/2468141085-swagger.PNG)

### Who do I talk to? ###

* Repo owner or admin
* franco.robert.fral@gmail.com